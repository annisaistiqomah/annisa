#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/gpio.h>
#include <linux/delay.h>  
#include <linux/init.h>
#include <linux/time.h>
#include <linux/device.h>
#include <linux/kdev_t.h>


int temp[400];
/* this structure maintain the information about DHT11 
 * including temperature and humidity that will show to the user space */ 
struct dht11_desc {
	int value;	
	int gpio_info;
	unsigned int temperature;	// 
	unsigned int humidity;		//
	unsigned long flags;   		// only FLAG_DHT11 is used, for synchronizing inside module
	#define FLAG_DHT11 0
	};
	
/* dht11_table
 * The table will hold a description for any GPIO pin available
 * on the system. It's wasteful to preallocate the entire table,
 * but avoiding race conditions is so much easier this way ;-)
*/	
static struct dht11_desc dht11_table[ARCH_NR_GPIOS];

/* lock protects against dht11_unexport() being called while
 * sysfs files are active.
 */
static DEFINE_MUTEX(sysfs_lock);

int dht11_export(unsigned gpio);   // forward definition
int dht11_unexport(unsigned gpio); // forward definition




/*show attribute value for DHT11 */
static ssize_t dht11_show(struct device *dev, struct device_attribute *attr, char *buf){
  const struct dht11_desc *desc = dev_get_drvdata(dev);
  ssize_t status;
  mutex_lock(&sysfs_lock);
  if(!test_bit(FLAG_DHT11, &desc->flags)){
    status = -EIO;
  }else{
    if(strcmp(attr->attr.name, "humidity")==0){
      status = sprintf(buf, "%d %\n", desc->humidity );
    }else if(strcmp(attr->attr.name, "temperature")==0){
      status = sprintf(buf, "%d C\n", desc->temperature);
    }else{
      status = -EIO;
    }
  }
  printk(KERN_INFO "alhamdulillah masuk");
  mutex_unlock(&sysfs_lock);
  return status;
}

/* Sysfs attributes definition for PWMs */
static DEVICE_ATTR(humidity,   0444, dht11_show,NULL);
static DEVICE_ATTR(temperature,0444, dht11_show,NULL);
static const struct attribute *dht11_dev_attrs[] = {
  &dev_attr_humidity.attr,
  &dev_attr_temperature.attr,
  NULL,
};
static const struct attribute_group dht11_dev_attr_group = {
  .attrs = (struct attribute **) dht11_dev_attrs,
};


/* Export a GPIO pin to sysfs, and claim it for DHT11 usage.
 * See the equivalent function in drivers/gpio/gpiolib.c
 */
static ssize_t export_store(struct class *class, struct class_attribute *attr, const char *buf, size_t len){
  long gpio;
  int  status;

  status = strict_strtol(buf, 0, &gpio);
  if(status<0){ goto done; }

  status = gpio_request(gpio, "DHT11");
  if(status<0){ goto done; }

  status = gpio_direction_output(gpio,0);
  if(status<0){ goto done; }
  
  status = dht11_export(gpio);
  if(status<0){ goto done; }

  set_bit(FLAG_DHT11, &dht11_table[gpio].flags);

done:
  if(status){
    gpio_free(gpio);
    pr_debug("%s: status %d\n", __func__, status);
  }
  return status ? : len;
}

/* Unexport a DHT11 GPIO pin from sysfs, and unreclaim it.
 * See the equivalent function in drivers/gpio/gpiolib.c
 */
static ssize_t unexport_store(struct class *class, struct class_attribute *attr, const char *buf, size_t len){
  long gpio;
  int  status;

  status = strict_strtol(buf, 0, &gpio);
  if(status<0){ goto done; }

  status = -EINVAL;
  if(!gpio_is_valid(gpio)){ goto done; }

  if(test_and_clear_bit(FLAG_DHT11, &dht11_table[gpio].flags)){
    status = dht11_unexport(gpio);
    if(status==0){ gpio_free(gpio); }
  }
done:
  if(status){ pr_debug("%s: status %d\n", __func__, status); }
  return status ? : len;
}


/* Sysfs definitions for dht11 class */
static struct class_attribute dht11_class_attrs[] = {
   __ATTR(export,   0200, NULL, export_store),
   __ATTR(unexport, 0200, NULL, unexport_store),
   __ATTR_NULL,
};

static struct class dht11_class = {
  .name =        "dht11",
  .owner =       THIS_MODULE,
  .class_attrs = dht11_class_attrs,
};

/* Setup the sysfs directory for a claimed dht11 device */
int dht11_export(unsigned gpio){
  struct dht11_desc *desc;
  struct device   *dev;
  int             status;

  mutex_lock(&sysfs_lock);

  desc = &dht11_table[gpio];
  desc->value  = 0;
  desc->humidity= 5;
  dev = device_create(&dht11_class, NULL, MKDEV(0, 0), desc, "pin%d", gpio);
	if(dev)
	{
		status = sysfs_create_group(&dev->kobj, &dht11_dev_attr_group);
		if(status==0)
			{
				desc->gpio_info = gpio;
				printk(KERN_INFO "Registered device dht11-%d\n", gpio);
			}
		else
			{
				device_unregister(dev);
			}
	}
	else
	{
		status = -ENODEV;
	}

  mutex_unlock(&sysfs_lock);

	if(status)
	{ 
		pr_debug("%s: dht11-pin%d status %d\n", __func__, gpio, status); 
	}
  return status;
}

/*
 *  Used by dht11_unexport below to find the device which should be freed */
static int match_export(struct device *dev, void *data){
  return dev_get_drvdata(dev) == data;
}

/* Free a claimed DHT11 device and unregister the sysfs directory */
int dht11_unexport(unsigned gpio){
  struct dht11_desc *desc;
  struct device   *dev;
  int             status;
      
  mutex_lock(&sysfs_lock);

  desc = &dht11_table[gpio];
  dev  = class_find_device(&dht11_class, NULL, desc, match_export);
  if(dev){
    put_device(dev);
    device_unregister(dev);
    printk(KERN_INFO "Unregistered device dht11%d\n", gpio);
    status = 0;
  }else{
    status = -ENODEV;
  }

  mutex_unlock(&sysfs_lock);

  if(status){ pr_debug("%s: dht11%d status %d\n", __func__, gpio, status); }
  return status;
}




static int __init gpio2_init(void)
{	
  int status;
  struct dht11_desc *desc;
  int gpio;
  
  printk(KERN_INFO "DHT11 initializing.\n");

  status = class_register(&dht11_class);
  if(status<0){ goto fail_no_class; }

  printk(KERN_INFO "DHT11 initialized.\n");
  return 0;

fail_no_class:
  return status;

}
	
static void __exit gpio2_exit(void)
{
	unsigned gpio;
	int status;
	
	
	for(gpio=0;gpio<ARCH_NR_GPIOS;gpio++)
	{
		struct dht11_desc *desc;
		desc = &dht11_table[gpio];
		if(test_bit(FLAG_DHT11,&desc->flags))
		{
			__gpio_set_value(gpio,0);
			status = dht11_unexport(gpio);
			if(status==0)
				{ 
					gpio_free(gpio); 
				}
		}
	}
	class_unregister(&dht11_class);
	printk(KERN_INFO "keluar\n");
}

module_init(gpio2_init);
module_exit(gpio2_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Atmel KP");
MODULE_DESCRIPTION("testing GPIO for DHT11");
